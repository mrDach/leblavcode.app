# How to install

Alternately, follow these steps:

	composer install
	npm install // for unix or "npm install --no-bin-links" for windows
	npm rebuild node-sass
	gulp

Enjoy! :)