
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the body of the page. From here, you may begin adding components to
 * the application, or feel free to tweak this setup for your needs.
 */
const Articles = require('./components/Articles.vue');
const Article = require('./components/Article.vue');
const Sources = require('./components/Sources.vue');

const router = new VueRouter({
    routes: [
        { path: '/', name: 'home', component: Articles },
        { path: '/article/:id', name: 'article', component: Article },
        { path: '/sources', name: 'sources', component: Sources }
    ]
});

const App = new Vue({
    components: {
        dropdown: VueStrap.dropdown,
        navbar: VueStrap.navbar
    },
    router
}).$mount('#app');
