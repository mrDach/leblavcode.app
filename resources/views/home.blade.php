@extends('layouts.app')

@section('content')
    <main v-on:scroll="onScroll">
        <div class="container">
            <div class="center-align">
                <h1>
                    Hello, {{Auth::user()->name}}
                </h1>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <articles></articles>
                </div>
            </div>
        </div>
    </main>
@endsection
