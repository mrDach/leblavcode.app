<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <navbar placement="top" type="default">
            <!-- Brand as slot -->
            <router-link slot="brand" class="navbar-brand" title="{{ config('app.name', 'Laravel') }}" :to="{ name: 'home'}">{{ config('app.name', 'Laravel') }}</router-link>
            @if (Auth::guest())
                <li slot="right"><a href="{{ route('login') }}">Login</a></li>
                <li slot="right"><a href="{{ route('register') }}">Register</a></li>
            @else
                <dropdown slot="right" text="{{ Auth::user()->name }}" type="default">
                    <li>
                        <router-link :to="{ name: 'sources'}">Sources</router-link>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}">Logout</a>
                    </li>
                </dropdown>
            @endif
        </navbar>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
