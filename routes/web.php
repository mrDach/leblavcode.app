<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Patterns
Route::pattern('id', '[0-9]+');
Route::pattern('page', '[0-9]+');
Route::pattern('limit',	'[0-9]{1,100}+');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/home',                     'HomeController@index')->name('home');
Route::get('/grab/{id}',                'GrabberController@grab')->name('grab');

Route::get('/article/{limit}/{page}',   'ArticleController@getPaged' )->name('article.list');
Route::get('/article/{id}',             'ArticleController@get' )->name('article.get');

Route::get('/source/{limit}/{page}',    'SourceController@getPaged' )->name('source.list');
Route::get('/source/{id}',              'SourceController@get' )->name('source.get');
Route::delete('/source/{id}',           'SourceController@delete' )->name('source.delete');
Route::post('/source',                  'SourceController@add' )->name('source.create');
Route::post('/source/{id}',             'SourceController@add' )->name('source.edit');
