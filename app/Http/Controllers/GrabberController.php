<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 06.08.17
 * Time: 19:11
 */

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Source;
use Symfony\Component\DomCrawler\Crawler;
use DB;

class GrabberController
{
    public function grab(int $id){

        DB::beginTransaction();
        try {

            $source = Source::find($id);

            $html = file_get_contents($source->url);

            $crawler = new Crawler($html);

            $posts = $crawler->filter($source->post);

            $posts->each(function ($node, $i) use($source) {
                $link = $node->filter($source->post_title);
                $title = $link->text();
                $url = $link->attr('href');
                $preView = $node->filter($source->pre_view)->html();
                if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
                    $url = str_replace("//", "", $url);
                    $url = "http://" . $url;
                }
                $content = new Crawler( file_get_contents( $url ) );
                $article = $content->filter($source->content)->html();
                Article::insertIgnore([
                    'title' => $title,
                    'pre_view' => $preView,
                    'content' => $article,
                ]);
            });

            DB::commit();
            //Return Success
            return ['result' => 1];

        } catch (\Exception $e) {

            DB::rollback();
            //Return Failure
            return ['result' => 0, 'errors' => [$e->getMessage()]];

        }

    }
}