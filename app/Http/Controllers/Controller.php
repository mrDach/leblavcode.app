<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use DB;
use Input;
use Validator;
use \Illuminate\Support\Facades\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    public $model;

    /**
     * get Models list
     * @param int $limit The Page Limit (Default: 0)
     * @param int $page Pages to Load (Default: 1)
     * @param \Illuminate\Database\Eloquent\Model $query Load Model with (Default: null)
     * @param array $columns (Default: '*')
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaged(int $limit = 0, int $page = 1, $query=null, array $columns = ['*'])
    {
        if(empty($query)){
            $query = $this->model;
        }
        if ($limit > 0) {
            $query = $query->take($limit)->skip((($page - 1) * $limit))->orderBy('id', 'desc');
            $total = $query->count();
            $data = $query->get($columns);
            //Return the Data
            return Response::json([
                'result' => 1,
                'total' => $total,
                'data' => $data
            ]);
        } else {
            //Get the All Models
            $data = $query->get();
            //Return Result
            return Response::json([
                'result' => 1,
                'data' => $data
            ]);

        }
    }

    /**
     * get Model by id
     * @param int $id Model id
     * @param \Illuminate\Database\Eloquent\Model $query Load Model with (Default: null)
     * @return \Illuminate\Http\JsonResponse
     */
    public function get(int $id, $query=null)
    {
        if(empty($query)){
            $query = $this->model;
        }
        try {
            //Get the Model
            if ($model = $query->find($id)) {
                //Return Result
                return Response::json([
                    'data' => $model,
                    'result' => 1,
                ]);
            }
            //Return Failed
            return Response::json([ 'result' => 0 , 'errors' => [ 'That Model Doesn\'t exist' ] ], 500);
        } catch (\Exception $e) {
            //Return Failure
            return Response::json([ 'result' => 0 , 'errors' => [ $e->getMessage() ] ], 500);
        }
    }

    /**
     * add or edit instance
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(){
        $data = call_user_func_array(['Input', 'only'], array_diff(
            array_merge($this->model->getFillable(),$this->model->getAttributes()), $this->model->getHidden()
        ));
        $validator_rules = $this->model->getValidatorRules($data['id']);
        $validator = Validator::make($data, $validator_rules, $this->model->messages);
        if( $validator->fails() ){
            //Return Failure
            return Response::json([ 'result' => 0 , 'errors' => $validator->errors()->getMessages() ], 400);
        }else{
            DB::beginTransaction();
            try {
                if($data['id']==''){
                    $data['id'] = null;
                }
                $this->model->updateOrCreate(['id' => $data['id']], $data);
                DB::commit();
                //Return Success
                return Response::json([ 'result' => 1 ]);
            } catch(\Exception $e ){
                DB::rollback();
                //Return Failure
                return Response::json([ 'result' => 0 , 'errors' => [ $e->getMessage() ] ], 500);
            }
        }
    }

    /**
     * delete Model
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(int $id){
        $validator = Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => "required|integer|exists:" . $this->model->getTable() . ",id",
            ]
        );
        if ($validator->fails()) {
            //Return Failure
            return Response::json([ 'result' => 0 , 'errors' => $validator->errors()->getMessages() ], 400);
        } else {
            DB::beginTransaction();
            try {
                $this->model->find($id)->delete();
                DB::commit();
                //Return Success
                return Response::json([ 'result' => 1 ]);
            } catch (\Exception $e) {
                DB::rollback();
                //Return Failure
                return Response::json([ 'result' => 0 , 'errors' => [ $e->getMessage() ] ], 500);
            }
        }
    }
}
