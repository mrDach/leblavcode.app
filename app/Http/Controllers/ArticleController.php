<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 07.08.17
 * Time: 12:26
 */

namespace App\Http\Controllers;

use App\Models\Article;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->model = new Article();
    }

    public function getPaged(int $limit = 0, int $page = 1, $query=null, array $columns = ['*'])
    {
        return parent::getPaged($limit, $page, null, ['id','title','pre_view']);
    }
}