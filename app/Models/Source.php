<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 06.08.17
 * Time: 20:21
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Source extends Model
{
    protected $table = 'sources';
    protected $fillable = ['id','title','url','post','post_title','pre_view','content'];

    /**
     * return rules for validation
     * @param string $id Model id
     * @return array
     */
    public function getValidatorRules($id = '')
    {
        return [
            'id'            => "",
            'title'         => "required|unique:{$this->table},title,{$id}",
            'url'           => "required",
            'post'          => "required",
            'post_title'    => "required",
            'pre_view'      => "required",
            'content'       => "required",
        ];
    }

    /**
     * @var array
     */
    public $messages = [
        'unique'    => ':attribute not unique',
        'required'  => ':attribute required',
    ];
}