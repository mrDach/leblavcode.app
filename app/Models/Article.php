<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 06.08.17
 * Time: 20:21
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['title','pre_view','content'];

    public static function insertIgnore($array){
        $a = new static();
        DB::insert('INSERT IGNORE INTO '.$a->table.' ('.implode(',',array_keys($array)).
            ') values (?'.str_repeat(',?',count($array) - 1).')',array_values($array));
    }

}